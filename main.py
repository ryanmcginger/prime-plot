#!/usr/bin/env python
#
# Script make all the plots!
#
import numpy as np
import primeplot
from tqdm import tqdm
from joblib import Parallel, delayed
from multiprocessing import cpu_count
import s3fs
import os

_cores = cpu_count()

p_arr = primeplot.makelist()

def run_one(inputs, s3=True):
    i, a, j, s = inputs
    primeplot.make_plot(
        primeplot.makeline(p_arr, step=0.1, theta=a*np.pi),
        # r"$\frac{{{x}\pi}}{{{y}}}$".format(x=i,y=f_base),
        r"${x:0.4f}\pi - {y:0.2f}$".format(x=a, y=s),
        f"{i:02d}-{j:02d}"
    )
    if s3:
        fs = s3fs.S3FileSystem()
        fs.put(
            f"imgs/{i:02d}-{j:02d}.png",
            f"s3://mcginger-data/primeplot/imgs/{i:02d}-{j:02d}.png"
        )
        os.remove(f"imgs/{i:02d}-{j:02d}.png")

def main():
    """
    """


    f_base = 12
    total = 3

    input_list = []

    for i,a in enumerate(
            np.linspace(1/f_base, (f_base-1)/f_base, num=total)
        ):
        for j, s in enumerate(np.linspace(0.01, 0.1, num=total)):
            input_list.append((i, a, j, s))

    foo = Parallel(n_jobs=_cores)(
        delayed(run_one)(i, s3=False) for i in tqdm(input_list, total=len(input_list))
    )


if __name__ == '__main__':
    main()