# prime-plot

Just some fun with primes and plotting.

## Setup

Dependencies are managed with pipenv:

```bash
pipenv install
```

## Run

Make the images:

```bash
mkdir imgs
./main.py
```

Make the gif:

```bash
ffmpeg -framerate 60 -pattern_type glob -i 'imgs/*.png' -r 15 -vf scale=512:-1 out.gif
```