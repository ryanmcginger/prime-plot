#
# Prime plot
#
import numpy as np
from numba import njit
import matplotlib.pyplot as plt

plt.style.use('dark_background')

@njit
def isprime(n):
    """
    Returns True if n is prime.
    """
    if n == 2:
        return 1
    if n == 3:
        return 1
    if n % 2 == 0:
        return 0
    if n % 3 == 0:
        return 0

    i = 5
    w = 2

    while i * i <= n:
        if n % i == 0:
            return 0
        i += w
        w = 6 - w

    return 1

def makelist(n=10_000_000):
    """
    Function make a big primality array. 
    """
    return np.array([isprime(i) for i in range(n)], dtype=bool)

@njit
def makeline(p_bool_arr, step=1.0, theta=np.pi/2):
    """
    Function to iterative build the points of line that changes direction with
    every prime.
    """

    n = p_bool_arr.shape[0]
    pts = np.zeros((n, 2), dtype=np.float64)
    pts[0, 0] = 0.5
    pts[0, 1] = 0.5

    dir_a = step
    dir_b = 0.0

    for i in range(1, n):
        if p_bool_arr[i]:
            a = dir_a * np.cos(theta) - dir_b * np.sin(theta)
            b = dir_a * np.sin(theta) + dir_b * np.cos(theta)
            dir_a = a
            dir_b = b

        pts[i, 0] = (pts[i-1, 0] + dir_a*step) % 1.0
        pts[i, 1] = (pts[i-1, 1] + dir_b*step) % 1.0

    return pts

def make_plot(pts_arr, title, fn, directory='imgs/'):
    """
    Function to plot the prime line.
    """

    fig, ax = plt.subplots(1, 1)
    ax.set_axis_off()
    ax.set_xlim(0, 1)
    ax.set_ylim(0, 1)
    ax.set_title(f"{title}")

    ax.scatter(
        pts_arr[:, 0],
        pts_arr[:, 1],
        c='w',
        s=0.1,
        # alpha=0.5,
        marker='.',
        edgecolors='none',
    )

    fig.savefig(
        f"{directory}{fn}.png",
        bbox_inches='tight',
        dpi=600
    )

    plt.cla()
    plt.clf()
    plt.close('all')
